package main

import (
	"context"
	"net/http"
	"os"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	log "github.com/sirupsen/logrus"
	tmhttp "github.com/tendermint/tendermint/rpc/client/http"
)

var (
	lastBlockNumber = promauto.NewGauge(prometheus.GaugeOpts{
		Name: "last_block_number",
		Help: "Last block number",
	})
	lastBlockTimeDifference = promauto.NewGauge(prometheus.GaugeOpts{
		Name: "last_block_difference",
		Help: "Last block difference",
	})
	numPeers = promauto.NewGauge(prometheus.GaugeOpts{
		Name: "num_peers",
		Help: "Peers count",
	})
)

func recordMetrics() {

	go func() {
		for {
			time.Sleep(2 * time.Second)

			c, err := tmhttp.NewWithClient("http://127.0.0.1:26657", "/", http.DefaultClient)

			if err != nil {
				log.Errorf("failed to create rpc client: %v", err)
				continue
			}

			ctx, _ := context.WithTimeout(context.Background(), time.Second*time.Duration(2))
			status, err := c.Status(ctx)
			if err != nil {
				log.Errorf("Failed to get status: %v", err)
				continue
			}

			lastBlockNumber.Set(float64(status.SyncInfo.LatestBlockHeight))

			lastBlockTimeDifference.Set(time.Now().Sub(status.SyncInfo.LatestBlockTime).Seconds())

			ctx, _ = context.WithTimeout(context.Background(), time.Second*time.Duration(2))
			net_info, err := c.NetInfo(ctx)

			if err != nil {
				log.Errorf("Failed to get net info: %v", err)
				continue
			}

			numPeers.Set(float64(net_info.NPeers))
		}
	}()
}

func init() {
	log.SetFormatter(&log.JSONFormatter{})
	log.SetOutput(os.Stdout)
	log.SetLevel(log.WarnLevel)
}

func main() {
	recordMetrics()
	http.Handle("/metrics", promhttp.Handler())
	http.ListenAndServe(":2112", nil)
}
